﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class BoardPiece
	{ // the mother class for everything that can be on the game board
		public static int size = 25;
		public int x;
		public int y;
		public Color color;

		public BoardPiece(int x, int y, Color color)
		{
			this.x = x;
			this.y = y;
			this.color = color;
		}

		public void draw(Canvas canvas)
		{
			Paint p = new Paint();
			p.Color = this.color;
			canvas.DrawRect(this.x, this.y, this.x + BoardPiece.size, this.y + BoardPiece.size, p);
		}
	}
}