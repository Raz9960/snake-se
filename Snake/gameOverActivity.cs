﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	[Activity(Label = "gameOverActivity")]
	public class gameOverActivity : Activity
	{
		// manges game over page
		Button btnHome;
		TextView tvScore;
		TextView tvBestScore;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.gameOver);
			btnHome = (Button)FindViewById(Resource.Id.btnHome);
			tvScore = (TextView)FindViewById(Resource.Id.tvScore);
			tvBestScore = (TextView)FindViewById(Resource.Id.tvBestScore);

			btnHome.Click += BtnHome_Click;
			
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
			ISharedPreferencesEditor editor = prefs.Edit();
			int bestScore = prefs.GetInt("bestScore", 0); // get current best score

			int score = Intent.GetIntExtra("score", 0); // get last games score
			if (!Game.isSinglePlayer) // if multyplayer displays which player wins
			{
				int score2 = Intent.GetIntExtra("score2", 0);
				tvScore.Text = score > score2 ? "Red wins!" : "Black wins!";
				if (score == score2)
					tvScore.Text = "Tie";
			}
			else // just displays score
				tvScore.Text = score.ToString();

			// update best score
			tvBestScore.Text = (score > bestScore ? score : bestScore).ToString();
			bestScore = score > bestScore ? score : bestScore;
			editor.PutInt("bestScore", bestScore);
			editor.Apply();

		}

		private void BtnHome_Click(object sender, EventArgs e)
		{
			Intent intent = new Intent(this, typeof(MainActivity));
			StartActivity(intent);
		}
	}
}