﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class Direction
	{
		public static int up = -1;
		public static int down = 1;
		public static int left = -2;
		public static int right = 2;
	}
}