﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class Cherry : BoardPiece
	{ // cherry class keeps cherry's score amount and responsible to move the cherry to a new location when needed
		public static bool isOnSnake = true;
		public static int score = 10;

		public Cherry(int x, int y, Color color) : base(x, y, color)
		{
			Random rnd = new Random();
			this.x = rnd.Next(0, Settings.width);
			this.y = rnd.Next(0, Settings.height);
			// make sure the cherry is on the grid
			this.x = Settings.AttachToGrid(this.x);
			this.y = Settings.AttachToGrid(this.y);
		}

		public void Eaten()
		{ // moves cherry to a new location
			Random rnd = new Random();
			this.x = rnd.Next(0,Settings.width);
			this.y = rnd.Next(0,Settings.height);
			// make sure the cherry is on the grid
			this.x = Settings.AttachToGrid(this.x);
			this.y = Settings.AttachToGrid(this.y);

			Cherry.isOnSnake = true; // turn on cherry might be on snake indicator so it could be moved again if necessery
		}
	}
}