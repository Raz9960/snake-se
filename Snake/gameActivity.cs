﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Preferences;

namespace Snake
{
	[Activity(Label = "gameActivity")]
	public class gameActivity : Activity
	{
		// gameActivity holds the game canvas and keeps track of the score
		bool run = true;

		TextView tvScore;
		LinearLayout gameLayout;
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			SetContentView(Resource.Layout.game_layout);
			this.gameLayout = (LinearLayout)FindViewById(Resource.Id.gameLayout);
			this.tvScore = (TextView)FindViewById(Resource.Id.tvScore);
			Game.isOver = false;
			Game.score = 0;
			Game.score2 = 0;

			// set background in case it is changed
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
			this.gameLayout.SetBackgroundColor(Android.Graphics.Color.ParseColor(prefs.GetString("backgroundColor", "#aa66cc")));

			// set difficulty
			Settings.difficulty = prefs.GetInt("difficulty", 2);

			Thread t = new Thread(myUpdate);
			t.Start();
		}

		private void myUpdate()
		{ // updates score and moving to new activity when game ends
			while (!Game.isOver && this.run)
				tvScore.Text = Game.score.ToString() + (Game.isSinglePlayer?"":" "+Game.score2);
			Intent intent = new Intent(this, typeof(gameOverActivity));
			intent.PutExtra("score", Game.score);
			intent.PutExtra("score2", Game.score2);
			StartActivity(intent);
		}

		protected override void OnDestroy()
		{
			this.run = false;
			base.OnDestroy();
		}
	}
}