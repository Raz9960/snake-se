﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	[Activity(Label = "level_design_activity")]
	public class level_design_activity : Activity
	{
		// manges the settings page
		Button btnPlay;
		Button btnPlayerMode;
		TextView menuChooseColor;
		TextView menuChooseDifficulty;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.level_design);

			this.btnPlay = (Button)FindViewById(Resource.Id.btnPlay);
			this.btnPlayerMode = (Button)FindViewById(Resource.Id.btnPlayerMode);
			this.menuChooseColor = (TextView)FindViewById(Resource.Id.menuPlayerColor);
			this.menuChooseDifficulty = (TextView)FindViewById(Resource.Id.menuDifficulty);

			this.btnPlayerMode.Click += BtnPlayerMode_Click;
			this.btnPlay.Click += BtnPlay_Click;
			RegisterForContextMenu(this.menuChooseColor);
			RegisterForContextMenu(this.menuChooseDifficulty);
		}

		private void BtnPlayerMode_Click(object sender, EventArgs e)
		{
			Game.isSinglePlayer = !Game.isSinglePlayer;
			this.btnPlayerMode.Text = Game.isSinglePlayer ? "SinglePlayer" : "multiplayer";
		}

		private void BtnPlay_Click(object sender, EventArgs e)
		{
			Intent intent = new Intent(this, typeof(gameActivity));
			StartActivity(intent);
		}

		public override bool OnContextItemSelected(IMenuItem item)
		{
			bool isColorMenu = true;
			string color = "";
			switch (item.ItemId)
			{
				case Resource.Id.red:
					color = "#cc0000";
					break;
				case Resource.Id.blue:
					color = "#0099cc";
					break;
				case Resource.Id.green:
					color = "#99cc00";
					break;
				case Resource.Id.purple:
					color = "#aa66cc";
					break;
				case Resource.Id.orange:
					color = "#ff8800";
					break;
				default:
					isColorMenu = false;
					break;
			}
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
			ISharedPreferencesEditor editor = prefs.Edit();
			if (isColorMenu)
				editor.PutString("backgroundColor", color);
			else // if not color menu then it is a difficulty menu
			{
				int difficulty = 2;
				switch (item.ItemId)
				{
					case Resource.Id.easy:
						difficulty = 1;
						break;
					case Resource.Id.normal:
						difficulty = 2;
						break;
					case Resource.Id.hard:
						difficulty = 3;
						break;
				}
				editor.PutInt("difficulty", difficulty);
			}

			editor.Apply();

			return true;
		}

		public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
		{
			base.OnCreateContextMenu(menu, v, menuInfo);
			if (v.Id == Resource.Id.menuPlayerColor)
				MenuInflater.Inflate(Resource.Menu.color_menu, menu);
			else if (v.Id == Resource.Id.menuDifficulty)
				MenuInflater.Inflate(Resource.Menu.difficulty_menu, menu);
		}
	}
}