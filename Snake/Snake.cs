﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class Snake
	{
		// player class responsible for moving and growing player's snake
		public BodyPart[] body;
		int length;
		public Head head;

		public Snake(int x, int y, Color color)
		{ // create snake
			this.length = 10;
			this.body = new BodyPart[this.length];
			this.head = new Head(x, y, color, Direction.left); // create head
			this.body[0] = this.head;
			for (int i = 1; i < this.length; i++) // create body
				this.body[i] = new BodyPart(x + i*BoardPiece.size, y, color);
		}

		public void draw(Canvas canvas)
		{
			foreach (BodyPart part in this.body)
				part.draw(canvas);
		}

		public void eat(Cherry cherry)
		{ // calls grow which makes the snake larger and activate cherry eaten function which moves it to a new position
			grow();
			cherry.Eaten();
		}

		public bool HitOther(Snake other)
		{ // checks if this snake hit another snakes body
			bool hit = false;
			foreach (BodyPart part in other.body)
				if (this.head.x == part.x && this.head.y == part.y)
					hit = true;
			return hit;
		}

		public void grow()
		{ // makes the snake larger by one body piece
			BodyPart[] previousBody = this.body;
			this.body = new BodyPart[++this.length];
			for (int i = 1; i < this.length; i++) // copy previous body into a bigger bodyPart array with room for a new part
				this.body[i] = previousBody[i - 1];

			// adds the new body part in a location according to the direction the snake is headed to 
			if (this.head.direction == Direction.up || this.head.direction == Direction.down) 
				this.head = new Head(this.head.x, this.head.y + this.head.direction * BoardPiece.size, this.head.color, this.head.direction);
			else if (this.head.direction == Direction.left || this.head.direction == Direction.right)
				this.head = new Head(this.head.x + (this.head.direction /2) * BoardPiece.size, this.head.y, this.head.color, this.head.direction);

			this.body[0] = this.head;
		}

		public void move()
		{
			// shift all body parts one position
			for (int i = this.length - 1; i > 0; i--)
				this.body[i] = this.body[i-1];

			// create a new part according to the direction the snake is headed
			if (this.head.direction == Direction.up || this.head.direction == Direction.down)
				this.head = new Head(this.head.x, this.head.y + this.head.direction * BoardPiece.size, this.head.color, this.head.direction);
			else if (this.head.direction == Direction.left || this.head.direction == Direction.right)
				this.head = new Head(this.head.x + (this.head.direction / 2) * BoardPiece.size, this.head.y, this.head.color, this.head.direction);
			this.body[0] = this.head;
		}
	}
}