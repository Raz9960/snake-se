﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class Head : BodyPart
	{
		// head is a special body part that keeps the direction the player is going to move towards
		public int direction;

		public Head(int x, int y, Color color, int direction) : base(x, y, color)
		{
			this.direction = direction;
		}
	}
}