﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	[BroadcastReceiver(Enabled = true)]
	[IntentFilter(new[] { Intent.ActionBatteryChanged })]
	public class BatteryReceiver : BroadcastReceiver
	{
		public static Context context;

		public BatteryReceiver()
		{ }

		public override void OnReceive(Context context, Intent intent)
		{
			int batteryPercent = intent.GetIntExtra("level", 0);
			if(batteryPercent < 50)
			{
				// battery is low, activate dialog to close app
				AlertDialog.Builder builder = new AlertDialog.Builder(BatteryReceiver.context);
				builder.SetTitle("Go out?");
				builder.SetMessage("I know the game is good but your battery is low\nMaybe leave your phone for a bit?");
				builder.SetCancelable(false);
				builder.SetPositiveButton("Yea you're right", OkAction);
				builder.SetNegativeButton("Naaa I want to play some more", DeclineAction);
				AlertDialog dialog = builder.Create();
				dialog.Show();
			}
		}

		private void DeclineAction(object sender, DialogClickEventArgs e)
		{
			Toast.MakeText(BatteryReceiver.context, "ok...", ToastLength.Short).Show();
		}

		private void OkAction(object sender, DialogClickEventArgs e)
		{
			Game.isOver = true;
			Android.OS.Process.KillProcess(Android.OS.Process.MyPid()); // kill app
		}
	}
}