﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class Game : View
	{
		// all game logic, game loop and variables
		Paint paint;
		Context context;
		Snake player;
		Snake player2;
		Cherry cherry;
		float downX;
		float downY;
		float upX;
		float upY;
		int lastSwipe = Direction.left;
		int count;
		public static bool isOver = false;
		public static bool isSinglePlayer = true;
		public static int score = 0;
		public static int score2 = 0;

		public Game(Context context, IAttributeSet attrs) :
			base(context, attrs)
		{
			//Initialize();
			this.context = context;
			this.paint = new Paint();
			this.paint.Color = Color.Red;
			this.count = 0;
		}

		public override bool OnTouchEvent(MotionEvent e)
		{

			Console.WriteLine("swipe: " + lastSwipe);
			switch (e.Action)
			{
				case MotionEventActions.Down:
					this.downX = e.GetX();
					this.downY = e.GetY();
					break;
				case MotionEventActions.Up:
					//Console.WriteLine("up " + e.GetX());
					this.upX = e.GetX();
					this.upY = e.GetY();

					//if multiplayer decide which player to move by the y coordinate of the first touch on screen
					Snake movedPlayer;
					if (Game.isSinglePlayer)
						movedPlayer = this.player;
					else
						movedPlayer = this.downY > (Settings.height / 2) ? this.player : this.player2;

					// if the differnce in height is bigger then in the width it means I should move the snake vertically
					if (Math.Abs((this.downY - this.upY)) > Math.Abs((this.downX - this.upX)))
					{
						if (this.upY > this.downY && movedPlayer.head.direction != Direction.up)
							movedPlayer.head.direction = Direction.down;
						else if(movedPlayer.head.direction != Direction.down)
							movedPlayer.head.direction = Direction.up;
					}
					else // horizontaly
					{
						if (this.upX > this.downX && movedPlayer.head.direction != Direction.left)
							movedPlayer.head.direction = Direction.right;
						else if (movedPlayer.head.direction != Direction.right)
							movedPlayer.head.direction = Direction.left;
					}
					break;
			}

			return true;
		}

		private void Setup(Canvas canvas)
		{ // sets canvas and creates all game objects
			Settings.width = canvas.Width;
			Settings.height = canvas.Height;
			this.player = new Snake(Settings.AttachToGrid(canvas.Width / 2), Settings.AttachToGrid(3*canvas.Height / 4), Color.Purple);
			if (!Game.isSinglePlayer)
				this.player2 = new Snake(Settings.AttachToGrid(canvas.Width / 2), Settings.AttachToGrid(canvas.Height / 4), Color.Black);
			this.cherry = new Cherry(0, 0, Color.Red);
		}

		protected override void OnDraw(Canvas canvas)
		{ // game loop
			if (player == null)
				Setup(canvas);
			
			// every time count is bigger then max speed the game will proggress in order to keep it running in a certain speed
			// speed will go up faster by score and increses faster by dependen
			count += Settings.gameSpeed + (Game.isSinglePlayer ? Settings.difficulty * Game.score/Cherry.score : 0);
			if (count > Settings.maxSpeed)
			{
				count = 0;
				//first player
				this.player.move();
				if (this.player.head.x == this.cherry.x && this.player.head.y == this.cherry.y)
				{
					this.player.eat(this.cherry);
					Game.score += Cherry.score;
				}
				//second player
				else if(!Game.isSinglePlayer)
				{
					this.player2.move();
					if (this.player2.head.x == this.cherry.x && this.player2.head.y == this.cherry.y)
					{
						this.player2.eat(this.cherry);
						Game.score2 += Cherry.score;
					}
				}
			}

			do
			{
				validateCherryPos();
			} while (Cherry.isOnSnake); // make sure cherry is not on top of the player body

			drawAll(canvas);

			// check if needs to end the game
			if (Game.isSinglePlayer)
				Game.isOver = gameOver(this.player); 
			else
			{
				Game.isOver = Game.score >= 100 || Game.score2 >= 100; 
				// if multyplayer needs to check if each player needs to reset and lose points
				if (gameOver(this.player) || this.player.HitOther(this.player2))
				{
					this.player = new Snake(Settings.AttachToGrid(canvas.Width / 2), Settings.AttachToGrid(3 * canvas.Height / 4), Color.Purple);
					Game.score -= Cherry.score;
				}
				if (gameOver(this.player2) || this.player2.HitOther(this.player))
				{
					this.player2 = new Snake(Settings.AttachToGrid(canvas.Width / 2), Settings.AttachToGrid(canvas.Height / 4), Color.Black);
					Game.score2 -= Cherry.score;
				}
			}

			this.Invalidate();
		}

		private void validateCherryPos()
		{ // make sure the cherry isnt on a snake
			Cherry.isOnSnake = false;
			foreach (BodyPart part in this.player.body)
			{
				if (this.cherry.x == part.x && this.cherry.y == part.y)
					this.cherry.Eaten();
			}
			if (!Game.isSinglePlayer)
			{
				foreach (BodyPart part in this.player.body)
				{
					if (this.cherry.x == part.x && this.cherry.y == part.y)
						this.cherry.Eaten();
				}
			}
		}

		private bool gameOver(Snake currentPlayer)
		{ // checks id player is out of bounds or hit it self
			if (currentPlayer.head.x > Settings.width || currentPlayer.head.x < 0 || currentPlayer.head.y > Settings.height || currentPlayer.head.y < 0)
				return true;
			foreach (BodyPart part in currentPlayer.body)
				if (part != currentPlayer.head)
					if (currentPlayer.head.x == part.x && currentPlayer.head.y == part.y)
						return true;
			return false;
		}

		private void drawAll(Canvas canvas)
		{
			this.player.draw(canvas);
			if (!Game.isSinglePlayer)
				this.player2.draw(canvas);
			this.cherry.draw(canvas);
		}
	}
}