﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	public class Settings
	{
		// game settings variables
		public static int width = 1080;
		public static int height = 1920;
		public static int maxSpeed = 100;
		public static int gameSpeed = 34;
		public static int difficulty = 2; // 1-eazy 2-normal 3-hard

		// changes given coordinate to a point on the grid
		// maybe should move to boardPiece
		public static int AttachToGrid(int x)
		{
			return x / BoardPiece.size * BoardPiece.size;
		}
	}
}