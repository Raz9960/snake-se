﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;
using System.Threading;
using Android.Preferences;

namespace Snake
{
    [Activity(Label = "@string/app_name", Theme = "@style/Theme.Design.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
		// manages the main page
		TextView tvScore;
		Button btnPlay;
		Button btnDesign;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

			BatteryReceiver.context = this;

			SetContentView(Resource.Layout.activity_main);

			this.btnPlay = (Button)FindViewById(Resource.Id.btnPlay);
			this.btnDesign = (Button)FindViewById(Resource.Id.btnDesign);
			this.tvScore = (TextView)FindViewById(Resource.Id.tvScore);

			this.btnPlay.Click += BtnPlay_Click;
			this.btnDesign.Click += BtnDesign_Click;

			//load best score
			ISharedPreferences prefs = PreferenceManager.GetDefaultSharedPreferences(this);
			this.tvScore.Text = prefs.GetInt("bestScore", 0).ToString();

			//starting service that will check on the battery and tell the player take a break
			Intent intent = new Intent(this, typeof(checkOnBattery));
			StartService(intent);
		}

		private void BtnDesign_Click(object sender, System.EventArgs e)
		{
			Intent intent = new Intent(this, typeof(level_design_activity));
			StartActivity(intent);
		}

		//move to the game activity to start the game with current settings
		private void BtnPlay_Click(object sender, System.EventArgs e)
		{
			Intent intent = new Intent(this, typeof(gameActivity));
			StartActivity(intent);
		}
	}
}