﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Snake
{
	[Service]
	public class checkOnBattery : Service
	{ // checks battary status and alerts player to take a break when needed
		BatteryReceiver batteryReceiver;
		public override void OnCreate()
		{
			this.batteryReceiver = new BatteryReceiver();
			base.OnCreate();
		}

		[return: GeneratedEnum]
		public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
		{
			RegisterReceiver(batteryReceiver, new IntentFilter(Intent.ActionBatteryChanged));
			return base.OnStartCommand(intent, flags, startId);
		}		

		public override void OnDestroy()
		{
			Toast.MakeText(this, "Ok I understand I am not your mom\nYou really should get out of your phone and meet some people though", ToastLength.Long).Show();
			UnregisterReceiver(batteryReceiver);
			base.OnDestroy();
		}

		public override IBinder OnBind(Intent intent)
		{
			return null;
		}
	}
}